FROM alpine

ARG TERRAFORM_VERSION

RUN apk add --no-cache ca-certificates \
      && apk add --no-cache -t deps curl bash \
      && curl -sL https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -o /tmp/terraform.zip \
      && unzip /tmp/terraform.zip -d /usr/local/bin/ \
      && chmod +x /usr/local/bin/terraform \
      && apk del --purge deps \
      && rm /tmp/terraform.zip
